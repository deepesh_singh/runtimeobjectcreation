
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

public class CmdInstantiate {

	//This utility method returns Object array of the classes passed to the function
    public Object[] initiate(String[] args) {
        Scanner in = new Scanner(System.in);
        Class cls = this.getClass();
        Object[] O = new Object[args.length];
        for (int count = 0; count < args.length; count++) {
            try {
                cls = Class.forName(args[count]);			//loading the class
            } catch (ClassNotFoundException cnfe) {
                System.out.println("Wrong Class Name");
            }

            Constructor[] cons = cls.getConstructors(); 	//reading all the public construvtors of the class
            System.out.println("Select Constructor: (Y for using, N for rejecting)");
            int i = 0;
            while (i < cons.length) {
                System.out.println((i + 1) + ". " + cons[i]);
                if (in.next().equalsIgnoreCase("Y")) {
                    Constructor con = cons[i];
                    Class[] c = con.getParameterTypes();	//reading parameter list's type of the constructor
                    i = 0;
                    System.out.println("Enter values for Parameter List: ");
                    Object[] arr = new Object[c.length];
                    while (i < c.length) {

                        System.out.println("Enter a/an "+c[i]+" value: ");
                        String name = c[i].getName();
                        arr[i] = in.next();
                        if (name.equals("int")) {
                            arr[i] = Integer.parseInt(arr[i].toString());	// Initializing parameter values
                            c[i] = Integer.TYPE;
                        } else if (name.equals("java.lang.String")) {
                            c[i] = String.class;
                        }
                        else if (name.equals("float")) {
                            arr[i] = Integer.parseInt(arr[i].toString());
                            c[i] = Integer.TYPE;
                        }

                        i++;
                    }
                    try {
                        //O = create(in, c, cls, arr, con);
                        O[count] = con.newInstance(arr);				//Calling constructor's newInstance method to call constructor
                    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        System.out.println(ex.getMessage());
                    }
                    System.out.println(O[count]);
                    break;
                }
                i++;
            }
        }
        return O;		//returning Object Array
    }
}