public class LauncherForInstantiate{
	public static void main(String[] args) {
		CmdInstantiate cmd=new CmdInstantiate();
		if(args.length>0)
			cmd.initiate(args);
		else
			System.out.println("No Class Names found.");	
	}
}